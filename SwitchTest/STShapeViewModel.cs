﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Drawing;
using System.Collections.Generic;

using Foundation;
using UIKit;

namespace SwitchTest
{
	public class STShapeViewModel
	{
		/// <summary>
		/// STShapeViewModel generates the views required for the main view controller.
		/// </summary>
		/// <remarks>
		/// It uses Shape-inherited model classes as a data source.
		/// </remarks>

		private Queue<UIView> squareViewQueue;
		private Queue<UIView> circleViewQueue;

		static STShapeViewModel singleton;

		public static STShapeViewModel SharedInstance {
			// This class should only have one instance
			get {
				if (singleton == null)
					singleton = new STShapeViewModel();
				return singleton;
			}
		}

		private STShapeViewModel ()
		{
			// Queues for pre-loading images so the user 
			// doesn't have to wait for a download on each tap
			this.squareViewQueue = new Queue<UIView>();
			this.circleViewQueue = new Queue<UIView>();
		}
			
		public async Task<UIView> GetShape ()
		{
			Random rand = new Random ();
			UIView shapeView;

			// Random choice between square or circle
			if (rand.Next(0, 2) == 0)
				shapeView = await this.GetCircle ();
			else
				shapeView = await this.GetSquare ();

			return shapeView;
		}		

		public async Task<UIView> GetSquare ()
		{
			// If we have already preloaded square views in the queue
			// then use those, otherwise re-load the queue and also get a
			// single square view.

			if (this.squareViewQueue.Count > 0) {
				return squareViewQueue.Dequeue();
			} else {	
				this.QueueSquareViews (Constants.ITEMS_DEFAULT_QUEUE);
				return await this.GenerateSquareView ();
			}
		}		

		public async Task<UIView> GetCircle ()
		{
			// If we have already preloaded circle views in the queue
			// then use those, otherwise re-load the queue and also get a
			// single circle view.

			if (this.circleViewQueue.Count > 0) {
				return circleViewQueue.Dequeue ();
			} else {	
				this.QueueCircleViews (Constants.ITEMS_DEFAULT_QUEUE);
				return await this.GenerateCircleView ();
			}
		}		
	
		private async Task QueueSquareViews (int NumberOfSquares)
		{
			for (int i = 0; i < NumberOfSquares; i++)
			{
				this.squareViewQueue.Enqueue (await this.GenerateSquareView ());
			}	
		}


		private async Task QueueCircleViews (int NumberOfCircles)
		{
			for (int i = 0; i < NumberOfCircles; i++)
			{
				this.circleViewQueue.Enqueue(await this.GenerateCircleView());
			}	
		}

		private async Task<UIView> GenerateSquareView ()
		{
			// Returns a square view w/ a image from a server,
			// or if an image is not available, a randomly generated 
			// background color.

			var square = new Square ();

			if (await square.GetImage ()) {
				var squareView = new UIImageView (square.image);
				squareView.UserInteractionEnabled = true;
				squareView.Tag = Constants.SQUARE_TAG;
				return squareView;
			} else {
				// If getting data from the server failed, generate
				// a square view using a random color
				var squareView = new UIView ();
				squareView.BackgroundColor = square.GetRandomColor ();
				squareView.Tag = Constants.SQUARE_TAG;
				return squareView;
			}
		}

		private async Task<UIView> GenerateCircleView ()
		{

			// Returns a square view w/ a color from a server,
			// or if that is not available, a randomly generated 
			// background color.

			var circle = new Circle ();
			UIView circleView = new UIView ();
			if (await circle.FillColor ()) 
			{	
				circleView.BackgroundColor = circle.color;
			} 
			else 
			{
				// If getting data from the server failed, generate
				// a circle view using a random color
				circleView.BackgroundColor = circle.GetRandomColor();
			}	
			circleView.Tag = Constants.CIRCLE_TAG;
			circleView.Alpha = 0.5f;
			circleView.Layer.CornerRadius = 50;
			return circleView;
		}
	}
}

