﻿using System;

using Foundation;
using UIKit;
using SwitchTest;
using CoreGraphics;

using System.Drawing;
using System.Collections.Generic;

namespace SwitchTest
{
	public partial class STMainViewController : UIViewController
	{
		/// <summary>
		/// STMainViewController responds to user input to place/modify views onto the screen.
		/// </summary>
		/// <remarks>
		/// It uses STShapeViewModel as the data source.
		/// </remarks>
	
		private RectangleF originalImageFrame = RectangleF.Empty;
		UIActivityIndicatorView activitySpinner;

		public STMainViewController () : base ("STMainViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		
			// Activity spinner to show when images are loading
			this.activitySpinner = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.WhiteLarge);
			this.activitySpinner.BackgroundColor = UIColor.DarkGray;
			this.activitySpinner.AutoresizingMask = UIViewAutoresizing.FlexibleMargins;
			View.AddSubview (this.activitySpinner);
		}
			

		public async override void TouchesEnded(NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);

			UITouch touch = touches.AnyObject as UITouch;

			this.activitySpinner.Frame = new CGRect ( 
			touch.LocationInView (View),
			new CoreGraphics.CGSize (this.activitySpinner.Frame.Width, this.activitySpinner.Frame.Height));
			this.activitySpinner.StartAnimating ();
	
			// Get a square or circle view from our view model
			var model = STShapeViewModel.SharedInstance;
			var view = await model.GetShape ();

			// Add a gesture recognizer to our new view that allows the user to drag the image
			UIPanGestureRecognizer pan = new UIPanGestureRecognizer ();
			pan.AddTarget (() => HandleDrag (pan));
			view.AddGestureRecognizer (pan);

			// Add a gesture recognizer to our new view that allows the user to double tap the image
			// to re-load a new one
			UITapGestureRecognizer tap = new UITapGestureRecognizer ();
			tap.NumberOfTapsRequired = 2;
			tap.AddTarget (() => HandleTap (tap));
			view.AddGestureRecognizer (tap);

			view.Frame = new CoreGraphics.CGRect (touch.LocationInView (View),
				new CoreGraphics.CGSize
				(Constants.SHAPE_DEFAULT_WIDTH, Constants.SHAPE_DEFAULT_HEIGHT));
			
			View.AddSubview (view);

			this.activitySpinner.StopAnimating ();
		}
			
		private void HandleDrag(UIPanGestureRecognizer recognizer)
		{
			UIView selectedView = recognizer.View;
			View.BringSubviewToFront (selectedView);

			// If the drag has just began, cache the location of the image
			if (recognizer.State == UIGestureRecognizerState.Began)
			{
				originalImageFrame = (RectangleF)selectedView.Frame;
			}

			// Move the image if the gesture is valid
			if (recognizer.State != (UIGestureRecognizerState.Cancelled | UIGestureRecognizerState.Failed
				| UIGestureRecognizerState.Possible))
			{
				// Move the image by adding the offset to the object's frame
				PointF offset = (PointF)recognizer.TranslationInView(selectedView);
				RectangleF newFrame = (RectangleF)originalImageFrame;
				newFrame.Offset(offset.X, offset.Y);
				selectedView.Frame = newFrame;
			}
		}

		private async void HandleTap(UITapGestureRecognizer recognizer)
		{
			// Remove the view and put a new one (with a new image or color) in its place 

			UIView selectedView = recognizer.View;
			UIView newView = new UIView ();

			var model = STShapeViewModel.SharedInstance;

			if (selectedView.Tag == Constants.SQUARE_TAG)
			{	
				newView = await model.GetSquare ();
			}
			else
			{
				newView = await model.GetCircle ();
			}
				
			newView.Frame = selectedView.Frame;

			selectedView.RemoveFromSuperview ();

			UIPanGestureRecognizer pan = new UIPanGestureRecognizer ();
			pan.AddTarget (() => HandleDrag (pan));
			newView.AddGestureRecognizer (pan);

			UITapGestureRecognizer tap = new UITapGestureRecognizer ();
			tap.NumberOfTapsRequired = 2;
			tap.AddTarget (() => HandleTap (tap));
			newView.AddGestureRecognizer (tap);
			View.AddSubview (newView);
		}
		}
	}


