﻿using System;

using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Xml;
using System.Collections.Generic;

using Foundation;
using UIKit;

namespace SwitchTest
{
	public partial class Square : Shape
	{
		/// <summary>
		/// Square gets the network data required for the view model to display square views.
		/// </summary>
		/// <remarks>
		/// It currently retrieves XML and images from a server to get the data.
		/// </remarks>

		private String imageUrl;
		public UIImage image;
		public UIImage latestDownloadedImage;

		public Square ()
		{
		}

		public async Task<Boolean> GetImage ()
		{

			if (await this.DownloadImage ()) 
			{	
				this.image = this.latestDownloadedImage;
				return true;
			} 
			else 
			{
				return false;
			}
		}


		private async Task<Boolean> DownloadImage ()
		{
			if (await GetImageUrl ()) 
			{
				try 
				{
					// Asynchronously get an image from the server and when that completes,
					// update this.latestDownloadedImage
					var httpClient = new HttpClient ();
					Task<byte[]> contentsTask = httpClient.GetByteArrayAsync (this.imageUrl);
					var contents = await contentsTask;
					this.latestDownloadedImage = UIImage.LoadFromData (NSData.FromArray (contents));	
					return true;
				} 
				catch (WebException ex) 
				{
					// If the image download didn't work, return false to
					// indicate that this.latestDownloadedImage has not
					// been updated
					return false;
				}
			} 
			else 
			{
				return false;
			}
		}
			
		private async Task<Boolean> GetImageUrl ()
		{
			if (await this.GetResponse (Constants.SQUARE_URL)) 
			{	
				// Asynchronously get an image URL from the server and when that completes,
				// update this.imageUrl
				XmlDocument xmlDoc = new XmlDocument ();
				xmlDoc.LoadXml (this.response);

				this.imageUrl = xmlDoc.SelectSingleNode (Constants.SQUARE_XML_PATH).FirstChild.Value;
				return true;
			} 
			else 
			{
				// If fetching the image URL didn't work, return false to
				// indicate that this.latestDownloadedImage has not
				// been updated
				return false;
			}
		
		}
			
	}
}

