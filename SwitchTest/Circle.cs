﻿using System;

using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Xml;

using Foundation;
using UIKit;

namespace SwitchTest
{
	public partial class Circle : Shape
	{
		/// <summary>
		/// Circle gets the network data required for the view model to display circle views.
		/// </summary>
		/// <remarks>
		/// It currently retrieves rgb color values (in XML format) from a server.
		/// </remarks>


		public Circle ()
		{
		}

		public async Task<Boolean> FillColor ()
		{
			if (await this.GetResponse (Constants.CIRCLE_URL)) 
			{
				// Asynchronously get the color triplet from the server and when that completes,
				// update this.color
				XmlDocument xmlDoc = new XmlDocument ();
				xmlDoc.LoadXml (this.response);

				int red = Convert.ToInt32 (xmlDoc.SelectSingleNode (Constants.CIRCLE_XML_PATH_RED).InnerXml);
				int green = Convert.ToInt32 (xmlDoc.SelectSingleNode (Constants.CIRCLE_XML_PATH_GREEN).InnerXml);
				int blue = Convert.ToInt32 (xmlDoc.SelectSingleNode (Constants.CIRCLE_XML_PATH_BLUE).InnerXml);
				this.color = new UIColor (red / 255f, green / 255f, blue / 255f, 255f / 255f);
				return true;
			} 
			else 
			{
				// If fetching the color triplet didn't work, return
				// false to indicate that this.color wasn't updated
				return false;	
			}	
		}
			
	}
}

