﻿using System;

using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Xml;

using Foundation;
using UIKit;

namespace SwitchTest
{
	public class Shape
	{
		/// <summary>
		/// Shape is the parent class for a variety of subclasses which get view-data from a server.
		/// </summary>
		/// <remarks>
		/// It also contains helper methods, for example to generate random colors when the server is
		/// unavailable.
		/// </remarks>



		public UIColor color;
		public String response;

		public Shape ()
		{
		}

		public UIColor GetRandomColor() 
		{
			Random rand = new Random();
			int hue = rand.Next(255);
			UIColor color = UIColor.FromHSB(
				(hue / 255.0f),
				1.0f,
				1.0f);
			return color;
		}

		protected async Task<Boolean> GetResponse (string url)
		{
			try
			{
				var httpClient = new HttpClient ();
				Task<String> dataTask = httpClient.GetStringAsync (url);
				this.response = await dataTask;
				return true;
			}

			catch(WebException ex) 
			{
				System.Console.WriteLine (ex.Message.ToString());
				return false;
			}
		}
	}


}

