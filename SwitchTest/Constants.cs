﻿using System;

namespace SwitchTest
{
	public static class Constants
	{
		public static int SQUARE_TAG { get { return 1; } }
		public static int CIRCLE_TAG { get { return 1; } }
		public static int SHAPE_DEFAULT_WIDTH { get { return 100; } }
		public static int SHAPE_DEFAULT_HEIGHT { get { return 100; } }
		public static int ITEMS_DEFAULT_QUEUE { get { return 20; } }

		public static String SQUARE_URL { get { return "http://www.colourlovers.com/api/patterns/random"; } } 
		public static String SQUARE_XML_PATH { get { return "patterns/pattern/imageUrl"; } } 

		public static String CIRCLE_URL { get { return "http://www.colourlovers.com/api/colors/random"; } } 
		public static String CIRCLE_XML_PATH_RED { get { return "colors/color/rgb/red"; } } 
		public static String CIRCLE_XML_PATH_GREEN { get { return "colors/color/rgb/green"; } } 
		public static String CIRCLE_XML_PATH_BLUE { get { return "colors/color/rgb/blue"; } } 
	}
}

