SwitchTest version 0.1 for iOS

**Release Notes**

25 May 2015

The SwitchTest iOS app currently has the following capabilities:

* User can touch the screen to display either a square or circle that is generated based on patterns and colors  that are retrieved from a server
* If the user is offline the shapes will be generated based on random colors
* Shapes can be dragged around on the screen
* Shapes can be double tapped to change pattern or color
* Shapes are all 100x100 pixels but size can be changed in the source code

Technical Info:

* Built in Xamarin 
* Uses asynchronous download patterns
* Uses image queues to save 20 images at one time locally for faster view loading
* Works on iOS 7+
* Requires Xamarin to build and compile